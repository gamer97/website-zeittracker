def getTime(start, end):
    start_data_hour = int(start[0:2])
    start_data_min = int(start[3:5])
    end_data_hour = int(end[0:2])
    end_data_min = int(end[3:5])

        
    min_data= end_data_min-start_data_min
    hour_data = end_data_hour-start_data_hour
    
    if min_data<0:
        min_data= min_data*-1
    if hour_data<0:
        hour_data = hour_data*-1
    
    if start_data_min>end_data_min:
        hour_data=hour_data-1
    
    return str(hour_data)+":"+str(min_data)

