from flask import Flask, render_template, url_for,request,redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import functions

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']= 'sqlite:///zeitdaten.db'
db= SQLAlchemy(app)

class ZeitDaten(db.Model):
    id= db.Column(db.Integer, primary_key=True)
    content= db.Column(db.String(200), nullable=False)
    date_created= db.Column(db.DateTime,default=datetime.utcnow)
    date_to_execute=db.Column(db.String(20))
    start_time= db.Column(db.String(5))
    end_time=db.Column(db.String(5))
    sum_time=db.Column(db.String(5))

    def __repr__(self):
        return '<Task %r>' % self.id



@app.route('/', methods=['POST','GET'])
def home():
    if request.method== 'POST':
        data_content = request.form['content']
        data_date = request.form['date']
        data_starttime = request.form['starttime']
        data_endtime = request.form['endtime']
        data_sumtime= functions.getTime(data_starttime,data_endtime)
        new_data= ZeitDaten(content= data_content, date_to_execute= data_date,start_time=data_starttime, end_time=data_endtime, sum_time=data_sumtime)
        try:
            db.session.add(new_data)
            db.session.commit() 
            return redirect('/')
        except Exception as e:
            return 'There was an issue adding your text'+ str(e)
    else:
        hours =ZeitDaten.query.order_by(ZeitDaten.date_created).all()
        return render_template('index.html',hours=hours)


@app.route('/delete/<int:id>')
def delete(id):
    data_to_delete = ZeitDaten.query.get_or_404(id)
    try:
        db.session.delete(data_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'Ein Fehler ist aufgetreten, die Aufgabe konnte nicht gelöscht werden.'

@app.route('/update/<int:id>',methods=['POST','GET'])
def update(id):
    data = ZeitDaten.query.get_or_404(id)
    if request.method == 'POST':
        data.content= request.form['content']
        data.date_to_execute= request.form['date']
        data.start_time= request.form['starttime']
        data.end_time = request.form['endtime']
        data.sum_time= functions.getTime(data.start_time,data.end_time)
        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'Ein Fehler ist aufgetreten, die Aufgabe konnte nicht geändert werden.'
    else:
        return render_template('update.html',hours=data)

if __name__== "__main__":
    app.run(debug=True)
        